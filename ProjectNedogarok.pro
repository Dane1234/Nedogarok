QT += core gui

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#TARGET = SplashScreenDemo
#TEMPLATE = app


#SOURCES += main.cpp

#HEADERS  +=
#FORMS    +=
#RESOURCES += \
 #  resources/res.qrc





QT += quick multimedia
CONFIG += c++11
QT += sql


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += C:/OpenSSL-Win32/includee

android { LIBS += -L$$PWD/android/lib/armv7-2/ -lcrypto
}

win32-g++{
LIBS += C:/OpenSSL-Win32/lib/libeay32.lib \
                        C:/OpenSSl-Win32/lib/ssleay32.lib
                        C:/OpenSSl-Win32/lib/ubsec.lib
}


# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    authcontroller.cpp \
    cryptocontroller.cpp \
    db_controller.cpp \
    tcpcontroller.cpp \
    friendsmodel.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    authcontroller.h \
    cryptocontroller.h \
    db_controller.h \
    tcpcontroller.h \
    friendsmodel.h
