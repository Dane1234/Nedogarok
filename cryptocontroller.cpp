#include "cryptocontroller.h"
#include <QFile>
#include <QIODevice>
#include <QDebug>

#include <openssl/conf.h> // функции, структуры и константы настройки OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> // сами криптогрфические функции https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровка
#include <openssl/aes.h>

cryptocontroller::cryptocontroller(QObject *parent) : QObject(parent)
{

}

void cryptocontroller::encryptFile(QString fileName, unsigned char *key, unsigned char *iv)
{
    QString in_file_name, out_file_name;
#ifdef Q_OS_ANDROID
    out_file_name = "/storage/emulated/0/Download/f_encrypted.txt";
    in_file_name = "/storage/emulated/0/Download/f0.txt";
#else
    out_file_name = "f_encrypted.txt";
    in_file_name = "f0.txt";
#endif

    //QString у key iv
    QFile f0(fileName),
            f_encrypted("f_encrypted.txt");
    f0.open(QIODevice::ReadOnly);//файл с исходными данными
    f_encrypted.open(QIODevice::WriteOnly | QIODevice::Truncate);//файл для зашифрованых данных
    char buffer[256] = {0};
    char out_buf[256] = {0};
    EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(),NULL,(unsigned char *)key,(unsigned char *)iv);
    //cbc независимое шифрование порций
    //iv вектор с рандомными числами для шифрования
    int len1 = 0;
    // Qfile read не только считывает данные но и двигает текущую позицию
    int len2 = f0.read(buffer, 256);
    while(len2 > 0)
    {
        EVP_EncryptUpdate(ctx, //обьект с настройками
                          (unsigned char *)out_buf, // входной параметр: ссылка, куда помещать
                          &len1, (unsigned char *)buffer, // выходной параметр: длина полученного шифра
                          len2);
        qDebug()<<"123";// входной параметр: длина входных данных
        f_encrypted.write(out_buf, len1); // вывод зашифрованной порции в файл
        qDebug()<<"456";
        len2 = f0.read(buffer, 256); // считываение следующей порции
    }
    EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf, &len1);
    f_encrypted.write(out_buf, len1);
    f_encrypted.close();
    f0.close();



    //инициализирующий вектор нужен для создания начального числа для шифрования vi
    //основная функция шифрования инкрипт апдейт шифрует данные порциями через буфер заданной длинны это значит что нужно сам файл считывать порциями от начала и до конца. размер буфера не кратен размеру файла. значит послений буфер будет заполнен не полностью.
    //чтобы коректно обработать последний не полный буфер, нужно коректно обработать последний блок
    //функции опен ssl расчитаны на с для большей переносимости и совместимости. значитт, что данные передаются в функции не в виде строк стринг, не в виде других объектов, а в виде массивов char.
    //а массивы char это и есть буфферы. должны быть без знаковыми
    // перед началом шифрования нужно настроить структуру EVP_CIPHER_CTX с помощью функции EVP_EncryptInit_ex
    // структура с настройками. заносятся метод шифрования,в виде массива ансайнгт чар заносится праоль

}

void cryptocontroller::decryptFile(QString fileName, unsigned char *key, unsigned char *iv)
{
    QString in_file_name, out_file_name;
#ifdef Q_OS_ANDROID
    out_file_name = "/storage/emulated/0/Download/f_encrypted.txt";
    in_file_name = "/storage/emulated/0/Download/f0.txt";
#else
    out_file_name = "f_encrypted.txt";
    in_file_name = "f0.txt";
#endif
    //QString у key iv
    QFile f1(fileName),
            f_encrypted("f_encrypted.txt");
    f_encrypted .open(QIODevice::ReadOnly);//файл с исходными данными
    f1.open(QIODevice::WriteOnly | QIODevice::Truncate);//файл для зашифрованых данных
    char buffer[256] = {0};
    char out_buf[256] = {0};
    qDebug()<<"*** before EVP_CIPHER_CTX_new";// входной параметр: длина входных данных
    EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();
    qDebug()<<"*** before EVP_DecryptInit_ex";// входной параметр: длина входных данных
    EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(),NULL,(unsigned char *)key,(unsigned char *)iv);
    //cbc независимое шифрование порций
    //iv вектор с рандомными числами для шифрования
    int len1 = 0;
    // Qfile read не только считывает данные но и двигает текущую позицию
    int len2 = f_encrypted.read(buffer, 256);
    qDebug()<<"*** before EVP_DecryptUpdate";// входной параметр: длина входных данных

    while(len2 > 0)
    {
        qDebug()<<"*** before EVP_DecryptUpdate";// входной параметр: длина входных данных

        EVP_DecryptUpdate(ctx, //обьект с настройками
                          (unsigned char *)out_buf, // входной параметр: ссылка, куда помещать
                          &len1, (unsigned char *)buffer, // выходной параметр: длина полученного шифра
                          len2);
        qDebug()<<"123";// входной параметр: длина входных данных
        qDebug()<<"*** out_buf"<<out_buf;
        f1.write(out_buf, len1); // вывод зашифрованной порции в файл
        qDebug()<<"456";
        len2 = f_encrypted.read(buffer, 256); // считываение следующей порции
    }
    qDebug()<<"*** before EVP_DecryptFinal_ex";// входной параметр: длина входных данных
    EVP_DecryptFinal_ex(ctx, (unsigned char *)out_buf, &len1);
    qDebug()<<"*** out_buf"<<out_buf;
    qDebug()<<"*** before f1.write";// входной параметр: длина входных данных
    f1.write(out_buf, len1);
    f1.close();
    f_encrypted.close();
    // EVP_EncryptFinal_ex


    //инициализирующий вектор нужен для создания начального числа для шифрования vi
    //основная функция шифрования инкрипт апдейт шифрует данные порциями через буфер заданной длинны это значит что нужно сам файл считывать порциями от начала и до конца. размер буфера не кратен размеру файла. значит послений буфер будет заполнен не полностью.
    //чтобы коректно обработать последний не полный буфер, нужно коректно обработать последний блок
    //функции опен ssl расчитаны на с для большей переносимости и совместимости. значитт, что данные передаются в функции не в виде строк стринг, не в виде других объектов, а в виде массивов char.
    //а массивы char это и есть буфферы. должны быть без знаковыми
    // перед началом шифрования нужно настроить структуру EVP_CIPHER_CTX с помощью функции EVP_EncryptInit_ex
    // структура с настройками. заносятся метод шифрования,в виде массива ансайнгт чар заносится праоль

}
