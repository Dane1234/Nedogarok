#include "friendsmodel.h"
#include <QDebug>

FriendObject::FriendObject(const QString &p_name,
const QString &p_photo,
const int &p_friend_id)
    : m_name(p_name),
m_photo(p_photo),
m_friend_id(p_friend_id)
{


}

QString FriendObject::getName() const
{
    return m_name;
}
QString FriendObject::getPhoto() const
{
    return m_photo;
}
int FriendObject::getFriend_id() const
{
    return m_friend_id;
}

FriendsModel::FriendsModel(QObject *parent)
    : QAbstractListModel(parent)
{
    //...
}

void FriendsModel::addItem(const FriendObject &newItem)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_items << newItem;
    endInsertRows();
}

int FriendsModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    return m_items.count();
}

QVariant FriendsModel::data(const QModelIndex & index,
                                int role) const
{
    if (index.row() < 0 || (index.row() >= m_items.count()))
        return QVariant();

    const FriendObject&itemToReturn = m_items[index.row()];
if (role == name)
return itemToReturn.getName();
else if (role == photo)
return itemToReturn.getPhoto();
else if (role == friend_id)
return itemToReturn.getFriend_id();


    return QVariant();
}

QHash<int, QByteArray> FriendsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[name] = "name";
    roles[photo] = "photo";
    roles[friend_id] = "friend_id";

    return roles;
}

QVariantMap FriendsModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void FriendsModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    m_items.clear();
    endRemoveRows();
}
