#ifndef CRYPTOCONTROLLER_H
#define CRYPTOCONTROLLER_H

#include <QObject>
#include <QFile>

class cryptocontroller : public QObject
{
    Q_OBJECT
public:
    explicit cryptocontroller(QObject *parent = nullptr);
    void encryptFile(QString fileName,
                     unsigned char * key,
                     unsigned char * iv);
    void decryptFile(QString fileName,
                     unsigned char * key,
                     unsigned char * iv);

signals:

public slots:
};

#endif // CRYPTOCONTROLLER_H
