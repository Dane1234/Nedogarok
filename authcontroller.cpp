#include "authcontroller.h"
#include <QDebug>
#include <QNetworkAccessManager> // класс клиента
#include <QNetworkReply> // класс инкапсуликующий ответ сервера в формате http
#include <QNetworkRequest> // класс инкапсуликующий запрос сервера в формате http
#include <QUrl> // класс, инкапсулирующий методы работы с URL (кодировки, парсинг) и общий функционал обычной строки
#include <QEventLoop>

#include <QMap>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QAbstractItemModel>

AuthController::AuthController(QObject *parent) : QObject(parent)
{
    f_model = new FriendsModel(this);

}

void AuthController::Authentificate(QString login, QString password)
{
    /*qDebug() << "login = " << login
                << "password = " << password;
       */
    QEventLoop loop;
    QNetworkAccessManager * na_manager = new QNetworkAccessManager(); // создание целого веб-клиента (открывает сокет,
    //привязывается к локальному IP, привязывается к порту http (80).

//6447927
    QString clientId = "6455595";
    //na_manager = new QNetworkAccessManager();
    connect(na_manager, SIGNAL(finished(QNetworkReply*)),
            &loop, SLOT(quit()));


    QNetworkReply * reply = na_manager->get(QNetworkRequest(QUrl("https://oauth.vk.com/authorize"
                                                                 "?client_id=" + clientId
                                                                 + "&display=mobile"        // отображение диалога для мобильных устройств
                                                                 "&redirect_uri=https://oauth.vk.com/blank.html" // официально рекомендовано https://vk.com/dev/implicit_flow_user
                                                                 "&scope=friends"           // Битовая маска настроек доступа приложения https://vk.com/dev/permissions
                                                                 "&response_type=token"     // что возвращать приложению
                                                                 "&v=5.37"                  // актуальная версия API VK
                                                                 "&state=6455595")));        // Произвольная строка,

    loop.exec();
    QString str( reply->readAll() );
    qDebug() << str;
    str.indexOf("");

    // Распарсивание формы авторизации

    // 1. Найти строку "_origin" и прочитать параметр за ней до след кавычки

    QString findStr = "_origin\" value=\"";
    int position1 = str.indexOf(findStr);
    position1 += findStr.length();
    int position2 = str.indexOf('\"', position1);
    QString parameter_origin = str.mid(position1, position2-position1);

    // 2. Аналогичным спомобом вытащить параметры "lg_h", "ip_h", "to"
    findStr = "lg_h\" value=\"";
    position1 = str.indexOf(findStr);
    position1 += findStr.length();
    position2 = str.indexOf('\"', position1);
    QString parameter_lg_h = str.mid(position1, position2-position1);

    findStr = "ip_h\" value=\"";
    position1 = str.indexOf(findStr);
    position1 += findStr.length();
    position2 = str.indexOf('\"', position1);
    QString parameter_ip_h = str.mid(position1, position2-position1);

    findStr = "to\" value=\"";
    position1 = str.indexOf(findStr);
    position1 += findStr.length();
    position2 = str.indexOf('\"', position1);
    QString parameter_to = str.mid(position1, position2-position1);

    /* Проверка
      qDebug() << parameter_origin;
      qDebug() << parameter_lg_h;
      qDebug() << parameter_ip_h;
      qDebug() << parameter_to;
      */

    reply = na_manager
            ->get(QNetworkRequest(
                      QUrl("https://login.vk.com/"
                           "?act=login"
                           "&soft=1"
                           "&utf8=1"
                           "&lg_h=" + parameter_lg_h
                           + "&ip_h=" + parameter_ip_h
                           + "&to=" + parameter_to
                           + "&_origin=" + parameter_origin
                           + "&email=" + login
                           + "&pass=" + password))); // TODO возможно, POST

    loop.exec();

    qDebug() <<  "*** РЕЗУЛЬТАТ 2 ЗАПРОСА HEADER " <<  reply->header(QNetworkRequest::LocationHeader).toString();
    qDebug() <<  "*** РЕЗУЛЬТАТ 2 ЗАПРОСА BODY " <<  reply->readAll();

    // Получаем редирект с успешной авторизацией
    reply = na_manager->get(
                QNetworkRequest(QUrl(
                                    reply->header(QNetworkRequest::LocationHeader).toString())));
    loop.exec();
    qDebug() <<  "*** РЕЗУЛЬТАТ 3 ЗАПРОСА HEADER " <<  reply->header(QNetworkRequest::LocationHeader).toString();
    qDebug() <<  "*** РЕЗУЛЬТАТ 3 ЗАПРОСА BODY " <<  reply->readAll();
    // Получаем редирект на токен, наш милый и любимый
    reply = na_manager->get(
                QNetworkRequest(
                    QUrl(
                        reply->header(QNetworkRequest::LocationHeader).toString())));
    loop.exec();

    str = reply->header(QNetworkRequest::LocationHeader).toString();
    qDebug() <<  "*** РЕЗУЛЬТАТ 4 ЗАПРОСА HEADER " << str;
    qDebug() <<  "*** РЕЗУЛЬТАТ 4 ЗАПРОСА BODY " << reply->readAll();

    if (str.indexOf("access_token") != -1)
    {
        m_accessToken = str.split("access_token=")[1].split("&")[0];
        emit authorized();
        qDebug() <<  "*** m_accessToken" << m_accessToken.mid(2,20);
    }
    else
        qDebug() << "Failed!";



    /* получение списка ID контактов ВК */
    reply = na_manager->get(
                QNetworkRequest(
                    QUrl("https://api.vk.com/method/friends.get"
                         "?out=0"
                         "&v=5.37"
                         "&fields=photo_100"
                         "&order=hints"
                         "&access_token=78ec78d032328b9c82458ab4a5cfa76a64b12b7b827c6ca0049d7d25ea6498be31de3a02d89aa4b01922"
                         // +" m_accessToken"
                         )));

    loop.exec();

    // считать в строку ответ сервера
    QString strFriends = reply->readAll();
    qDebug() << "*** friends.get = " << strFriends;



    // вся строка JSON грузится в QJsonDocument
    QJsonDocument itemDoc = QJsonDocument::fromJson(strFriends.toUtf8());
    QJsonObject itemObject = itemDoc.object();
    qDebug()<<"*** itemObject.keys()"<<itemObject.keys();

    QJsonObject responseObject;
    QJsonArray itemArray;

    if (itemObject.contains("response"))
    {
        responseObject = itemObject["response"].toObject();

    }
    else
    {

        return;
    }

    if (responseObject.contains("items"))
    {

        itemArray = responseObject["items"].toArray();
    }
    else
    {

        return;
    }

    qDebug () <<"*** itemArray[10] " << itemArray;

    for(int i = 0; i < itemArray.size(); i++)
    {
        QJsonObject f_object = itemArray[i].toObject();

        QString name = f_object["first_name"].toString();
        qDebug () << "*** name[" << i << "] = " << name;

        QString photo = f_object["photo_100"].toString();
        qDebug () << "*** photo[" << i << "] = " << photo;

        int id = f_object["id"].toInt();
        qDebug () << "*** f_object[" << i << "] = " << f_object;

        f_model->addItem(FriendObject(name, photo, id));
    }
}
