#include "tcpcontroller.h"

#include <QTcpServer>
/* класс, инкапсулирующий
 * 1) сокет (конечную точку с буфером, связанную с сетевым адаптером)
 * 2) бесконечный цикл попыток чтения из сокета (т.н. "прослушка")
 * 3) генерация сигналов при поступлении каких-то данных на сокет сервера
*/

#include <QTcpSocket>
/* класс, инкапсулирующий
 * 1) буфер данных, который будет отправляться/считываться с адаптера
 * 2) функция для чтения/записи
 * 3) остальной функционал типичного устройства ввода/вывода (наподобие QFile, QIODevice и т.д)
*/



/*
 * Цикл работы с tcp-сервером
 * 1) Создать объект
 * 2) Создать слот-обработчик данных сервера
 * 3) Привязать слот для обработки для поступающих данных
 *      QObject::connect(&имя_сервера, SIGNAL(finished()),
 *                       &имя_объекта, SLOT(...))
 * 4) Запустить "прослушку" serv.listen(ip-адрес, порт)
 *
 * Цикл работы с tcp-сокетом
 * 1) Создать сокет
 * 2) Отправить данные socket.write() либо считать socket.read()
*/

/*tcpcontroller::tcpcontroller(QObject *parent) : QObject(parent)
{
    QTcpServer serv;
    QTcpSocket sock;
    serv.listen(QHostAddress::LocalHost,
                33333); // запуск беск. цикла прослушки
}*/
tcpcontroller::tcpcontroller(QObject *parent) : QObject(parent)
{
    serv = new QTcpServer();
    client_sock = new QTcpSocket();
    serv_sock = new QTcpSocket();

    QObject::connect(serv, SIGNAL(newConnection()),
                     this,  SLOT(newConnection()));

    serv->listen(QHostAddress("127.0.0.1"),
                33333); // запуск бесконечного цикла прослушки

    QObject::connect(client_sock, SIGNAL(readyRead()),
                     this,  SLOT(client_sock_ready()));
    client_sock->connectToHost("127.0.0.1",33333);


}

void tcpcontroller::newConnection()
{
    //qDebug<<"*** newConnection()";
    serv_sock = serv->nextPendingConnection();
    connect(serv_sock, SIGNAL(readyRead()),
            this, SLOT(server_sock_ready()));
}

void tcpcontroller::server_sock_ready()
{
    // если сообщение короткое и уместилось в одну посылку
    // можно считать через client_sock->readAll()

    // если массив данных (>1Кб) передается по частям, то
    // 1. нужно отслеживать начало и конец сообщения
    // 2. считывать через client_sock->read()
    // 3. возможно нужно накапливать все фрагменты в одном буфере

    // TODO считать (если короткое сообщение - то client_sock->readAll())
    // TODO расшифровать
    // TODO расшифрованный текст отправить в ListView QML с помощью сигнала
    // как а ЛР по аутентификации
}

void tcpcontroller::client_sock_ready()
{

    // TODO считать (если короткое сообщение - то client_sock->readAll())
    // TODO расшифровать
    // TODO расшифрованный текст отправить в ListView QML с помощью сигнала
    // как а ЛР по аутентификации

}

void tcpcontroller::sendMessage()
{
      // TODO прописать отправку сигнала из QML и срабатывание данного слота
}
