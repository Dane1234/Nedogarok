#ifndef AUTHCONTROLLER_H
#define AUTHCONTROLLER_H

#include <QObject>
#include <QUrl>
#include <QNetworkAccessManager>
#include "friendsmodel.h"
class AuthController : public QObject
{
    Q_OBJECT
public:
    explicit AuthController(QObject *parent = nullptr);
    QNetworkAccessManager * na_manager = new QNetworkAccessManager(this);
    QNetworkReply * reply;
    QNetworkRequest request;
    QString response;
    QString m_accessToken;
    FriendsModel * f_model;
signals:
       void authorized();
public slots:
    void Authentificate(QString login, QString password);

};

#endif // AUTHCONTROLLER_H
