#ifndef TCPCONTROLLER_H
#define TCPCONTROLLER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class tcpcontroller : public QObject
{
    Q_OBJECT
public:
    explicit tcpcontroller(QObject *parent = nullptr);
    QTcpServer * serv;
    QTcpSocket * client_sock; // сокет, имитирующий соединение
    QTcpSocket * serv_sock;
signals:

public slots:
    void newConnection();
    void server_sock_ready();
    void client_sock_ready();
    void sendMessage();
};

#endif // TCPCONTROLLER_H
