import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtMultimedia 5.8
import QtGraphicalEffects 1.0
/*Справка http://doc.qt.io/qt-5/qtml-cppintegration-topic.html
    1.Привязка С++ слотов к QML-сигналам.
    2.Получение ссылок на QML-объекты в С++ коде (делает видимым QML-объекты в С++)
    3.Помещение С++ -функций и объектов в область видимости QML
    4.QML-слотов к сигналам С++ объектов
*/

ApplicationWindow {

    signal sendAuth(string login, string password);

    visible: true
    width: 640
    height: 480
    title: qsTr("Tabs")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        //Page1Form {
        //}
        Page {
            width: 600
            height: 400

            header: Label {
                text: qsTr("Аутенфикация")
                font.pixelSize: Qt.application.font.pixelSize * 2
                padding: 10
            }

            ColumnLayout{
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0
                anchors.fill : parent // выравнивание по краям
                width: 40 * Screen.pixelDensity
                height: 8 * Screen.pixelDensity

                Rectangle{
                    border.width: 1
                    border.color: "black"
                    Layout.preferredWidth: 40 * Screen.pixelDensity
                    Layout.preferredHeight: 8 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignHCenter

                    TextField {
                        id:edtLogin
                        placeholderText: "Введите имя"
                        text: ""
                        anchors.fill: parent
                        echoMode: "Password"
                    }
                }

                Rectangle{
                    border.width: 1
                    border.color: "black"
                    Layout.preferredWidth: 40 * Screen.pixelDensity
                    Layout.preferredHeight: 8 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignHCenter

                    TextField {
                        id:edtPswd
                        placeholderText: "Введите пароль"
                        text: ""
                        anchors.fill: parent
                        echoMode: "Password"
                    }
                }

                Button{
                    Layout.preferredWidth: 40 * Screen.pixelDensity
                    Layout.preferredHeight: 8 * Screen.pixelDensity
                    Layout.alignment: Qt.AlignHCenter
                    onClicked: {
                        sendAuth(edtLogin.text, edtPswd.text)
                    }
                    text: "Войти"

                    //кнопка ввода...

                }
            }
        }

        Page{
                    width: 600
                    height: 400
                    ListView{
                        id: listView
                        anchors.fill: parent
                        anchors.margins: 10
                        model : f_model
                        spacing : 40

                        delegate: Rectangle{
                            id: deleg_rectangle
                            width: listView.width
                            height: Screen.pixelDensity * 15
                            color: "white"
                            radius: 5
                            border.width: 3
                            border.color: "#00a6f9"

                            GridLayout{

                                anchors.fill: parent
                                Image {
                                    Layout.row: 0
                                    Layout.column: 0
                                    source: photo
                                }

                                Text{
                                    Layout.row: 0
                                    text: friend_id
                                    Layout.column: 1
                                }

                                Text{
                                    Layout.row: 1
                                    Layout.column: 1
                                    text:name
                                }
                            }
                        }


                    }
                }
        Page{
                    MediaPlayer{
                    //класс для открытия и обработки аудио-видео файлов
                        id:mediplayer
                        autoLoad: true//грузить файл сразу без запроса
                        loops:MediaPlayer.Infinite//число повторений
                        autoPlay: true
                        source: "file:///C:/progi/qt/project/sample.avi"

                    }//second media
                    Camera{
                    //класс для получения изображения с камеры и работа с ней
                        id: camera
                    }//second camera
                    ColumnLayout{
                           anchors.fill: parent
                           VideoOutput{
                               enabled: true
                               Layout.fillWidth: true
                               Layout.fillHeight: true
                               source: mediplayer//либо камера

                           }//second video
                            //Button{}
                           // Button{}
                    }//second layout


                }//вторая страница последняя скобка
        Page{
                    //camera
                    MediaPlayer{
                    //класс для открытия и обработки аудио-видео файлов
                        id:mediplayer1
                        autoLoad: true//грузить файл сразу без запроса
                        loops:MediaPlayer.Infinite//число повторений
                        autoPlay: true
                        source: "file:///C:/progi/qt/project/sample.avi"

                    }//second media
                    Camera{
                    //класс для получения изображения с камеры и работа с ней
                        id: camera1
                        //camera1image
                    }//second camera
                    ColumnLayout{
                           anchors.fill: parent
                           VideoOutput{
                               enabled: true
                               Layout.fillWidth: true
                               Layout.fillHeight: true
                               source: camera1//либо камера

                           }//second video
                            Button{
                            text:"сделать \nфото"
                            onClicked: {
                                camera1.imageCapture.captureToLocation;
                                camera1.imageCapture.capture();
                            }
                            }
                            Button{
                                text:"снять \nвиедео"
                                onClicked: {
                                    console.log(camera1.videoRecorder)

                                    if(camera1.videoRecorder.recorderState === CameraRecorder.RecordingState){
                                    camera1.videoRecorder.stop();
                                        console.log

                                    }

                                }


                            }


                            Button{
                                text:"остановить /nвиедео"
                                onClicked: {
                                    camera1.videoRecorder.stop();

                                }
                            }
                    }//second layout


                }//вторая страница последняя скобка



    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Аутенфикация")
        }


        TabButton {
            text: qsTr("Друзья из ВК")
        }
        TabButton {
            text: qsTr("Видео")
        }
        TabButton {
            text: qsTr("Камера")
        }
    }
}
//89687522584
//053380107a1
