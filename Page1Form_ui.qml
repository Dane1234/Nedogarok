import QtQuick 2.9
import QtQuick.Controls 2.2

Page {

    width: 600
    height: 400

    header: Label {
        text: "Страница авторизации"
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Button {
        id: button
        x: 370
        y: 130
        width: 130
        height: 60
        text: qsTr("Button")
    }

    TextEdit {
        id: textEdit
        x: 150
        y: 130
        width: 220
        height: 30
        text: "Логин"
        font.weight: Font.ExtraLight
        textFormat: Text.AutoText
        font.pixelSize: 12
    }

    TextEdit {
        id: textEdit1
        x: 150
        y: 160
        width: 220
        height: 30
        text: qsTr("Пароль")
        font.pixelSize: 12
    }
}
