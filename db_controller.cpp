#include "db_controller.h"
#include <QSqlDatabase>
#include <QSqlQuery> // Класс, инкапсулирующий:
                     //  1) строку SQL запроса,
                     //  2) сигналы и слоты готовности СУБД,
                     //  3) результат, возвращаемый СУБД,
                     //  4) коды ошибок
                     //  5) прочий второстепенный функционал

/* цикл работы с SQLite БД:
 * 1) создать интерфейс БД (для нужного драйвера - "QSQLITE", "MYSQL", либо др.)
 * 2) открыть с его помощью файл
 * 3) отправляются запросы
 *  3.1) формируется строка запроса
 *  3.2) query.exec()
 * 4) закрыть БД
 */

db_controller::db_controller(QObject *parent) : QObject(parent)
{


    db = QSqlDatabase::addDatabase("QSQLITE"); // тип БД

    db.setDatabaseName("fts.sqlite"); // имя файла БД

    db.open();

}


void db_controller::sendQuieries()
{

    QSqlQuery query ("CREATE TABLE friends"
                    "(userID int ,"
                    "userName varchar(255));");
     db.exec();

     query.prepare ("INSERT INTO friends (userID, userName) "
                    "VALUES (1, \'Ivan\');");
     query.exec();

     query.prepare ("INSERT INTO friends (userID, userName) "
                    "VALUES (2, \'Misha\');");
     query.exec();

     query.prepare ("INSERT INTO friends (userID, userName) "
                    "VALUES (3, \'Pasha\');");
     query.prepare ("INSERT INTO friends (userID, userName) "
                    "VALUES (4, \'saha\');");
     query.exec();

}

db_controller::~db_controller()
{

    db.close();

}
