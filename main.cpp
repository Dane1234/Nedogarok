#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "authcontroller.h"
#include "cryptocontroller.h"
#include <QQmlContext>
#include "db_controller.h"


int main( int argc, char* argv[] ) {

 


    QCoreApplication::setAttribute( Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    AuthController authController; // слот появляется здесь

    cryptocontroller cryptoController;
    db_controller database;
    database.sendQuieries();


    cryptoController.encryptFile("f0.txt",
                                 (unsigned char *)"123456789",
                                 (unsigned char *)"123456798");
    cryptoController.decryptFile("f1.txt",
                                 (unsigned char *)"123456789",
                                 (unsigned char *)"123456789");

    QQmlApplicationEngine engine; // создается движок-контейнер отрисовки QML

    QQmlContext *context = engine.rootContext();
    context->setContextProperty("auth.Controller", /*строковое имя для QML*/&authController); // перемещаемый объект
    // Именно эта функция помещает в контекст (область видимости)
    // QML сущность из С++

    engine.load(QUrl(QStringLiteral("qrc:/main.qml"))); // стартовый QML-файл

    if (engine.rootObjects().isEmpty())
        return -1;

    // сигнал может возникнуть потенциально начиная с этого момента

    // 1-й тип связывания: видимость QML-объекта из С++
    // можно вызывать ТОЛЬКО после загрузки движка и QML-файла
    // ссылка на QML-окно - обладатель сигнала

    QObject * appWindow = engine.rootObjects().first();

    QObject::connect(appWindow,
                     SIGNAL(sendAuth(QString, QString)), // чей и какой сигнал
                     &authController,
                     SLOT(Authentificate(QString, QString))); // к чьему и какому слоту
















    return app.exec();
}


