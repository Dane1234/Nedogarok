#ifndef FRIENDSMODEL_H
#define FRIENDSMODEL_H

#include <QAbstractListModel>
#include <QVariant>
#pragma once

class FriendObject
{
public:
    FriendObject(const QString &p_name,
                const QString &p_photo,
                const int &p_friend_id);

    QString getName() const;
    QString getPhoto() const;
    int getFriend_id() const;


private:
    QString m_name;
    QString m_photo;
    int m_friend_id;

};

class FriendsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum enmRoles {
        name = Qt::UserRole + 1,
        photo,
        friend_id
    };

    FriendsModel(QObject *parent = 0);

    void addItem(const FriendObject & newItem);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariantMap get(int idx) const;
    void clear();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<FriendObject> m_items;

};
#endif // FRIENDSMODEL_H
